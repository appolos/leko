import './Profile.scss';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from '../shared/Header/Header';
import Container from '../shared/Container/Container';
import profileSelector from './ducks/selectors';
import { startAsyncAction } from '../App/ducks/actions';
import { fetchUserData } from './ducks/actions';

export class Profile extends React.Component {
    componentDidMount() {
        const { appAsyncAction } = this.props;
        appAsyncAction();
    }

    render() {
        const { username, email, fetchUser } = this.props;
        return (
            <div className="profile-page">
                <Header title="Profile Page" />
                <Container title="Profile page content">
                    <div>
                        <span>Username: </span>
                        {username}
                    </div>
                    <div>
                        <span>Email: </span>
                        {email}
                    </div>
                    <div>
                        <button
                            className="async-action-button"
                            type="button"
                            onClick={fetchUser}
                        >
                            Change user data asynchronously
                        </button>
                    </div>
                </Container>
            </div>
        );
    }
}

Profile.propTypes = {
    email: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    fetchUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    email: profileSelector.getEmail(state),
    username: profileSelector.getUsername(state)
});

const mapDispatchToProps = dispatch => ({
    fetchUser: () => dispatch(fetchUserData()),
    appAsyncAction: () => dispatch(startAsyncAction())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
