import { put, takeLatest, delay } from 'redux-saga/effects';
import types from './actionTypes';
import { fetchedUserData, failedFetchUserData } from './actions';

function* profileAsyncAction() {
    try {
        // const { data } = yield api.get(apiUrls.users.list);
        delay(1000);
        yield put(fetchedUserData());
    } catch (err) {
        yield put(failedFetchUserData());
    }
}

const profileSaga = [takeLatest(types.FETCH_USER_DATA, profileAsyncAction)];

export { profileSaga };
