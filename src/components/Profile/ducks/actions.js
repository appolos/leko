import { createAction } from 'redux-actions';
import types from './actionTypes';

const fetchUserData = createAction(types.FETCH_USER_DATA);
const fetchedUserData = createAction(types.FETCHED_USER_DATA);
const failedFetchUserData = createAction(types.FAILED_FETCH_USED_DATA);

export { fetchUserData, fetchedUserData, failedFetchUserData };
