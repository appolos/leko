import { put, takeLatest, delay } from 'redux-saga/effects';

import { failedAsyncAction, finishAsyncAction } from './actions';
import types from './actionTypes';

function* asyncAction() {
    try {
        // const { data } = yield api.get(apiUrls.users.list);
        delay(1000);
        yield put(finishAsyncAction());
    } catch (err) {
        yield put(failedAsyncAction());
    }
}

const appSaga = [takeLatest(types.START_ASYNC_ACTION, asyncAction)];

export { appSaga };
