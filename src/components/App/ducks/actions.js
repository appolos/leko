import { createAction } from 'redux-actions';
import types from './actionTypes';

const startAsyncAction = createAction(types.START_ASYNC_ACTION);
const finishAsyncAction = createAction(types.FINISHED_ASYNC_ACTION);
const failedAsyncAction = createAction(types.FAILED_ASYNC_ACTION);

export { startAsyncAction, finishAsyncAction, failedAsyncAction };
