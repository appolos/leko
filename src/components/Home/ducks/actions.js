import { createAction } from 'redux-actions';
import types from './actionTypes';

const increaseCounter = createAction(types.HOME_INCREASE_COUNTER);
const decreaseCounter = createAction(types.HOME_DECREASE_COUNTER);

export { increaseCounter, decreaseCounter };
