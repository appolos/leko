import "./Home.scss";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Header from "../shared/Header/Header";
import Container from "../shared/Container/Container";
import { increaseCounter, decreaseCounter } from "./ducks/actions";
import selectors from "./ducks/selectors";
import { startAsyncAction } from "../App/ducks/actions";
import axios from 'axios'
class Home extends React.Component {
  componentDidMount() {
      this.fetchInst()
  }
  fetchInst = async () => {
    try {
      const data = await axios.get(`https://api.instagram.com/oauth/authorize?client_id=2861751107384913&redirect_uri=https://kole.netlify.com/&scope=user_profile,user_media&response_type=code`);
        console.log(data)
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    const {
      counter,
      increaseCounter,
      decreaseCounter,
      startAsyncAction
    } = this.props;
    return (
      <div className="home-page">
        <Header title="Home Page" />
        <Container title="Home page content">Test</Container>
      </div>
    );
  }
}

Home.propTypes = {
  counter: PropTypes.number.isRequired,
  increaseCounter: PropTypes.func.isRequired,
  decreaseCounter: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  counter: selectors.getCounter(state)
});

const mapDispatchToProps = dispatch => ({
  increaseCounter: () => dispatch(increaseCounter()),
  decreaseCounter: () => dispatch(decreaseCounter()),
  startAsyncAction: () => dispatch(startAsyncAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
