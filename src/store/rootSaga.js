import { all } from 'redux-saga/effects';
import { appSaga } from '../components/App/ducks/saga';
import { profileSaga } from '../components/Profile/ducks/saga';

export default function* rootSaga() {
    yield all([...appSaga]);
    yield all([...profileSaga]);
}
