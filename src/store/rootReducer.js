import { combineReducers } from 'redux';

import homeReducer from '../components/Home/ducks/reducer';
import appReducer from '../components/App/ducks/reducer';
import profileReducer from '../components/Profile/ducks/reducer';

const rootReducer = combineReducers({
    home: homeReducer,
    app: appReducer,
    profile: profileReducer
});

export default rootReducer;
